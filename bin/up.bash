#!/usr/bin/env bash

# CONFIGURAÇÕES BÁSICAS PARA A INICIALIZAÇÃO DE UM GERENCIADOR DE JANELAS.
# PROJETO: (https://gitlab.com/jcmljunior/better-wm)
# AUTOR: JULIO CESAR <jcmljunior@gmail.com>
# VERSÃO: 1.0.0

function desktop_init()
{
  [ ! -f "$PROJECT_PATH/sound/desktop-login.wav" ] && return 0
  sleep 5
  aplay -D sysdefault "$PROJECT_PATH/sound/desktop-login.wav"
}

function autostart()
{
	declare AUTOSTART_ENABLED="$(grep -Po 'AUTOSTART_ENABLED=\K.*' <<< "$PROJECT_CONF")"
  AUTOSTART_ENABLED="${AUTOSTART_ENABLED^^}"
  
	[[ "${AUTOSTART_ENABLED:-FALSE}" = 'FALSE' ]] && return 0
  
  declare -a AUTOSTART_APPS=($(find "${PROJECT_PATH}/autostart" -type f))

  for app in "${AUTOSTART_APPS[@]}"; do
    declare PROGRAM=$(grep -Po 'Exec=\K.*' "$app")
    pidof -s "$PROGRAM" || setsid -f "$PROGRAM"
  done >/dev/null 2>&1
}

function keyboard()
{
	declare KEYBOARD_ENABLED="$(grep -Po 'KEYBOARD_ENABLED=\K.*' <<< "$PROJECT_CONF")"
  KEYBOARD_ENABLED="${KEYBOARD_ENABLED^^}"

  [ "${KEYBOARD_ENABLED:-FALSE}" = 'FALSE' ] && return 0

  declare KEYBOARD_MODEL="$(grep -Po 'KEYBOARD_MODEL=\K.*' <<< "$PROJECT_CONF")"
  declare KEYBOARD_LAYOUT="$(grep -Po 'KEYBOARD_LAYOUT=\K.*' <<< "$PROJECT_CONF")"
  KEYBOARD_LAYOUT="${KEYBOARD_LAYOUT,,}"

  declare KEYBOARD_VARIANT="$(grep -Po 'KEYBOARD_VARIANT=\K.*' <<< "$PROJECT_CONF")"
  KEYBOARD_VARIANT="${KEYBOARD_VARIANT,,}"
  
  declare KEYBOARD_OPTIONS="$(grep -Po 'KEYBOARD_OPTIONS=\K.*' <<< "$PROJECT_CONF")"

  [[ ! -x "$(command -v setxkbmap)" ]] && {
		echo 'Oppss, não foi possível encontrar o setxkbmap.'
    exit 1
  }
  
  setxkbmap $(echo "${KEYBOARD_MODEL:+-model $KEYBOARD_MODEL} ${KEYBOARD_LAYOUT:+-layout $KEYBOARD_LAYOUT} ${KEYBOARD_VARIANT:+-variant $KEYBOARD_VARIANT} ${KEYBOARD_OPTIONS:+-options $KEYBOARD_OPTIONS}")
}

function touchpad()
{
	declare TOUCHPAD_ENABLED="$(grep -Po 'TOUCHPAD_ENABLED=\K.*' <<< "$PROJECT_CONF")"
  TOUCHPAD_ENABLED="${TOUCHPAD_ENABLED^^}"

	[ "${TOUCHPAD_ENABLED:-FALSE}" = 'FALSE' ] && return 0

  [[ ! -x "$(command -v xinput)" ]] && {
		echo 'Oppss, não foi possível encontrar o xinput.'
    exit 1
  }
  
  xinput set-prop 'SynPS/2 Synaptics TouchPad' 'libinput Tapping Enabled' 1
}

function wallpaper()
{
  declare WALLPAPER_ENABLED="$(grep -Po 'WALLPAPER_ENABLED=\K.*' <<< "$PROJECT_CONF")"
  WALLPAPER_ENABLED="${WALLPAPER_ENABLED^^}"

  [ "${WALLPAPER_ENABLED:-FALSE}" = 'FALSE' ] && return 0

  [[ ! -x "$(command -v feh)" ]] && {
    echo 'Oppss, não foi possível encontrar o feh.'
    return 1
  }

  declare WALLPAPER_PATH=$(find "$PROJECT_PATH/backgrounds" -type l,f | sort -R | tail -1)

  [ -z "$WALLPAPER_PATH" ] && {
    echo 'Oppss, não foi possível encontrar os plano de fundo.'
		return 1
  }

  feh --conversion-timeout 1 --bg-fill "$WALLPAPER_PATH"
}

function window_manager()
{
	declare WINDOW_MANAGER_SESSION=$(grep -Po 'WINDOW_MANAGER_SESSION=\K.*' <<< "$PROJECT_CONF")
  WINDOW_MANAGER_SESSION="${WINDOW_MANAGER_SESSION,,}"

  [[ ! -x "$(command -v "$WINDOW_MANAGER_SESSION")" ]] && {
		echo "Oppss, não foi possível encontrar ${WINDOW_MANAGER_SESSION:-undefined}."
    exit 1
  }

  declare WINDOW_MANAGER_OPTIONS="$(grep -Po 'WINDOW_MANAGER_OPTIONS=\K.*' <<< "$PROJECT_CONF")"
  WINDOW_MANAGER_OPTIONS="${WINDOW_MANAGER_OPTIONS/BASE_DIR/$PROJECT_PATH}"

  "$($WINDOW_MANAGER_SESSION $WINDOW_MANAGER_OPTIONS &2> /dev/null && {
    [ -f "$PROJECT_PATH/$WINDOW_MANAGER_SESSION/autoload.bash" ] && {
      . "$PROJECT_PATH/$WINDOW_MANAGER_SESSION/autoload.bash"
    }

		desktop_init
  })"
}

# A DECLARAÇÃO "PROJECT_PATH" DEFINE O CAMINHO ABSOLUTO PARA O DIRETÓRIO DO PROJETO.
declare PROJECT_PATH=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
PROJECT_PATH="${PROJECT_PATH/\/bin/}"

[ ! -f "$PROJECT_PATH/config.conf" ] && {
	echo 'Oppss, não foi possível encontrar o arquivo de configurações.'
  exit 1
}

# A DECLARAÇÃO "PROJECT_CONF" DEFINE O COMPORTAMENTO DOS COMPONENTES.
declare PROJECT_CONF=$(cat "$PROJECT_PATH/config.conf")

# A DECLARAÇÃO "PROJECT_ARR" DEFINE A ORDEM DOS COMPONENTES A SEREM INICIADAS POR PADRÃO.
declare -a PROJECT_ARR=(
  'keyboard'
  'touchpad'
	'wallpaper'
  'autostart'
  'window_manager'
)

# COMPONENTES EXTRAS.
. "$PROJECT_PATH/bash-toolkit/toolkit.bash"

# INICIA OS COMPONENTES VIA PARÂMETROS OU INICIA TODOS OS COMPONENTES DEFINIDOS EM $PROJECT_ARR.
if [ -z "$1" ]; then
	for str in "${PROJECT_ARR[@]}"; do
	  [[ -n "$(function_exists "$1")" ]] && {
      "$str"
    }
  done
elif [ -n "$1" ] && [[ -n "$(function_exists "$1")" ]]; then
  "$1" "${@:2}"
else
  echo 'Oppss, não foi possivel localizar a função solicitada.'
fi

echo 'Terminado.'
exit 0
